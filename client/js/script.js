// IFFE Expression is a javascript function that runs immediately 
// (immediately invoked functional expression)

(function() {
  var app = angular.module("RegApp", []); 

  // From app, call controller take in 2 parameters 
  // (RegistrationCtrl and dependencies to be injected) 
  app.controller("RegistrationCtrl", ["$http", RegistrationCtrl]);

  function RegistrationCtrl($http) {
    var self = this; // vm

    self.user = {
      email: "",
      password: ""
    };

    self.displayUser = {
      email: "",
      password: ""
    };

    self.registerUser = function() {
      console.log("self.user.email");
      console.log("self.user.password");
      $http.post("/users", self.user)
        .then(function(result) {
          console.log(result);
          self.displayUser.email = result.data.email;
          self.displayUser.password = result.data.password;
        })
        .catch(function(e) {
          console.log(e);
        });
    };

  }

}) ();